# Epam Orders

The exercise is to demo your technical skills and takes most candidates a few
hours; please don’t spend a huge amount of time making it perfect.

- Please complete the following task with the language and libraries you
feel comfortable with.

- Use git to version control the code and once complete, send us a link to
the Github (or similar) repository via the recruitment agent
Instructions: Complete the steps in order. At each step build the simplest
possible solution which meets the requirement. Tag and git commit after
each step so that your approach is clear.

## Step 1: Build an Orders Service :heavy_check_mark:
- [x] Build a service that’s able to receive simple orders of shopping goods via a REST API. 
- [x] Apples cost 60 cents and oranges cost 25 cents. 
- [x] The service should return a summary of the order, including the cost of the order. 
- [x] Add unit tests that validate your code.

## Step 2: Simple offer :heavy_check_mark:
- [x] The shop decides to introduce two new offers:
  - [x] Buy one get one free on Apples. 
  - [x] 3 for the price of 2 on Oranges. 
- [x] Update your functions & unit tests accordingly.

## Step 3: Store and retrieve orders :heavy_check_mark:
- [x] The service should now store the orders that a customer submits. 
- [x] There should be an endpoint to get a particular order based on its ID. 
- [x] There should be an endpoint to get all orders. 
- [x] This store does not have to be to disk. 
- [x] Update your functions & unit tests accordingly.
package com.epam.orders.service.orders.orders;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    @Test
    void testOrder() {
        // given
        int applesQuantity = 10;
        int orangesQuantity = 10;
        Order order = new Order(applesQuantity, orangesQuantity);
        // when
        int totalPrice = order.getTotalOrder();
        // then
        int expectedTotalPrice = applesQuantity * Order.APPLES_PRICE + order.getOrangesTotalPrice();
        assertEquals(expectedTotalPrice, totalPrice);
    }

    @Test
    void testToString() {
        // given
        int applesQuantity = 10;
        int orangesQuantity = 10;
        Order order = new Order(applesQuantity, orangesQuantity);
        // when
        String orderString = order.toString();
        // then
        assertEquals("Order{" +
                "applesQuantity=" + applesQuantity +
                ", orangesQuantity=" + orangesQuantity +
                ", applesTotalPrice=" + applesQuantity * Order.APPLES_PRICE +
                ", orangesTotalPrice=" + order.getOrangesTotalPrice() +
                ", freeApples=" + order.getFreeApples() +
                ", totalOrder=" + order.getTotalOrder() +
                '}', orderString);
    }

    @Test
    void getFreeApples() {
        // given
        int applesQuantity = 15;
        int orangesQuantity = 12;
        Order order = new Order(applesQuantity, orangesQuantity);
        // when
        int freeApples = order.getFreeApples();
        // then
        assertEquals(applesQuantity, freeApples);
    }

    @Test
    void getOrangesTotalPrice() {
        // given
        int applesQuantity = 15;
        int orangesQuantity = 12;
        Order order = new Order(applesQuantity, orangesQuantity);
        // when
        int orangesPrice = order.getOrangesTotalPrice();
        // then
        int expectedOrangePrice = 8 * Order.ORANGES_PRICE;
        assertEquals(expectedOrangePrice, orangesPrice);

        // given
        orangesQuantity = 13;
        order = new Order(applesQuantity, orangesQuantity);
        // when
        orangesPrice = order.getOrangesTotalPrice();
        // then
        expectedOrangePrice = 9 * Order.ORANGES_PRICE;
        assertEquals(expectedOrangePrice, orangesPrice);

        // given
        orangesQuantity = 14;
        order = new Order(applesQuantity, orangesQuantity);
        // when
        orangesPrice = order.getOrangesTotalPrice();
        // then
        expectedOrangePrice = 10 * Order.ORANGES_PRICE;
        assertEquals(expectedOrangePrice, orangesPrice);

        // given
        orangesQuantity = 0;
        order = new Order(applesQuantity, orangesQuantity);
        // when
        orangesPrice = order.getOrangesTotalPrice();
        // then
        expectedOrangePrice = 0;
        assertEquals(expectedOrangePrice, orangesPrice);
    }
}
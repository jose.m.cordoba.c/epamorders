package com.epam.orders.service.orders.orders;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OrderDAO {

    Order insertOrder(UUID id, Order order);

    default Order addOrder(Order order) {
        UUID id = UUID.randomUUID();
        return insertOrder(id, order);
    }

    List<Order> selectAllOrders();

    Optional<Order> selectOrderByID(UUID id);
}

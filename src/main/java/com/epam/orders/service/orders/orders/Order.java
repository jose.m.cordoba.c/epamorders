package com.epam.orders.service.orders.orders;

import javax.persistence.Transient;
import java.util.UUID;

public class Order {

    public static final int APPLES_PRICE = 60;
    public static final int ORANGES_PRICE = 25;

    private UUID Id;
    int applesQuantity;
    int orangesQuantity;

    public Order() {
    }

    public Order(UUID id, int applesQuantity, int orangesQuantity) {
        Id = id;
        this.applesQuantity = applesQuantity;
        this.orangesQuantity = orangesQuantity;
    }

    @Transient
    int applesTotalPrice;
    @Transient
    int orangesTotalPrice;
    @Transient
    int totalOrder;
    @Transient
    int freeApples;

    public Order(int applesQuantity, int orangesQuantity) {
        this.applesQuantity = applesQuantity;
        this.orangesQuantity = orangesQuantity;
        this.Id = UUID.randomUUID();
    }

    @Override
    public String toString() {
        return "Order{" +
                "applesQuantity=" + this.applesQuantity +
                ", orangesQuantity=" + this.orangesQuantity +
                ", applesTotalPrice=" + getApplesTotalPrice() +
                ", orangesTotalPrice=" + getOrangesTotalPrice() +
                ", freeApples=" + getFreeApples() +
                ", totalOrder=" + getTotalOrder() +
                '}';
    }

    public int getFreeApples() {
        return applesQuantity;
    }

    public void setFreeApples(int freeApples) {
        this.freeApples = freeApples;
    }

    public UUID getId() {
        return Id;
    }

    public int getApplesQuantity() {
        return applesQuantity;
    }

    public void setApplesQuantity(int applesQuantity) {
        this.applesQuantity = applesQuantity;
    }

    public int getOrangesQuantity() {
        return orangesQuantity;
    }

    public void setOrangesQuantity(int orangesQuantity) {
        this.orangesQuantity = orangesQuantity;
    }

    public int getApplesTotalPrice() {
        return APPLES_PRICE * this.applesQuantity;
    }

    public void setApplesTotalPrice(int applesTotalPrice) {
        this.applesTotalPrice = applesTotalPrice;
    }

    public int getOrangesTotalPrice() {
        int paidQuantity = this.orangesQuantity / 3 * 2 + this.orangesQuantity % 3;
        return ORANGES_PRICE * paidQuantity;
    }

    public void setOrangesTotalPrice(int orangesTotalPrice) {
        this.orangesTotalPrice = orangesTotalPrice;
    }

    public int getTotalOrder() {
        return getOrangesTotalPrice() + getApplesTotalPrice();
    }

    public void setTotalOrder(int totalOrder) {
        this.totalOrder = totalOrder;
    }
}

package com.epam.orders.service.orders.orders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class OrderService {

    private final OrderDAO orderDAO;

    @Autowired
    public OrderService(@Qualifier("fakeDAO") OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    public Order addNewOrder(Order order) {
        return orderDAO.addOrder(order);
    }

    public List<Order> getAllOrders() {
        return orderDAO.selectAllOrders();
    }

    public Optional<Order> getOrderById(UUID id){
        return orderDAO.selectOrderByID(id);
    }
}

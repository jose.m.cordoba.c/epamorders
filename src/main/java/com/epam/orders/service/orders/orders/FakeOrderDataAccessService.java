package com.epam.orders.service.orders.orders;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("fakeDAO")
public class FakeOrderDataAccessService implements OrderDAO {

    private static List<Order> DB = new ArrayList<>();

    @Override
    public Order insertOrder(UUID id, Order order) {
        Order newOrder = new Order(id, order.getApplesQuantity(), order.getOrangesQuantity());
        DB.add(newOrder);
        return newOrder;
    }

    @Override
    public List<Order> selectAllOrders() {
        return DB;
    }

    @Override
    public Optional<Order> selectOrderByID(UUID id) {
        return DB.stream()
                .filter(person -> person.getId().equals(id))
                .findFirst();
    }
}
